
Template Starter Module
------------------------
by logicdesign, info@logicdesign.be


Description
-----------
The objective of this module is to convert a custom html web page into a 
Drupal template.
This module will get an html file, analyse it and create the folder and 
files required to activate the template.

The aim of this module is not to provide an interface for ALL possible 
functionalities you can perform on a template but to allow an easy and 
fast template creation from an html page.

Installation 
------------

 * Copy the module's directory to your modules directory and activate the 
module.
 * Go to the configuration page of the module to start the template 
generation.
